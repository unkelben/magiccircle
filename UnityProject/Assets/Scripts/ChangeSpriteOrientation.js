﻿#pragma strict

public var rightfootstepobject:Transform;
public var leftfootstepobject:Transform;
public var rightfootstepposition:Transform;
public var leftfootstepposition:Transform;
public var guySprites:Texture2D[] ;
public var walkRight:Texture2D[];
public var walkLeft:Texture2D[];
public var walkFront:Texture2D[];
public var walkBack:Texture2D[];
public var electrocute:Texture2D[];
public var currentAnim:Texture2D[] = walkFront;
public var cam:Camera;
public var movescript:MoveAround;
public var leavingfootsteps:boolean = false;
public var animate:boolean = true;
public var animationSpeedModifier:float = 1f;

public var footstepSFX:AudioClip[];
public var hardStep:boolean = false;

public var curFrame:int = 0;
public var electrocuted:boolean = false;

function Start () {

	AnimateSprites();
	currentAnim = walkFront;

}

function AnimateSprites(){

	while(true==true){
		if (currentAnim == walkRight || currentAnim == walkLeft) {
			yield WaitForSeconds(0.05 / animationSpeedModifier);
		}
		else {
			yield WaitForSeconds(0.081 / animationSpeedModifier);
		}

		if (animate) {
		//Debug.Log("current anim length:"+currentAnim.length);
		if(movescript.moving||electrocuted){
			if(curFrame<(currentAnim.length-2)){
				curFrame++;
			}
			else {
				curFrame = 0;
			}
//			Debug.Log("About to handle audio...");
			HandleFootstepAudio();
		}
		else{
//			Debug.Log("Not moving in ChangeSpriteOrientation");
		    curFrame = currentAnim.length-1;
		}
		}
		else {
			curFrame = currentAnim.length-1;
		}

		GetComponent.<Renderer>().material.mainTexture = currentAnim[curFrame];

	}

}

function Update () {

 	//Which plane does the camera see, change sprite accordingly
 	var hit:RaycastHit;
 	var dir:Vector3 = gameObject.transform.position - GetComponent.<Camera>().main.transform.position;
 	if(Physics.Raycast(cam.transform.position, dir, hit, 1000)) 
	{
		if(hit.collider.name=="GuyFront")
			currentAnim = walkFront;
		if(hit.collider.name=="GuyBack")
			currentAnim = walkBack;
		if(hit.collider.name=="GuyLeft")
			currentAnim = walkLeft;
		if(hit.collider.name=="GuyRight")
			currentAnim = walkRight;
		//GetComponent.<Renderer>().material.mainTexture = currentAnim[0];
	 	//Debug.Log(hit.collider.name+", "+hit.collider.tag);
	}

	if(electrocuted){
		currentAnim = electrocute;
	}
		//GetComponent.<Renderer>().material.mainTexture = guySprites[0];


}


function HandleFootstepAudio () {

	var audio:AudioSource = gameObject.GetComponent(AudioSource);

	var footstepIndex:int;
	if (hardStep){
//		Debug.Log("HARD STEP");
		footstepIndex = 4;
	}
	else {
		footstepIndex = Random.Range(0,footstepSFX.Length-1);
	}

//	Debug.Log("Footstep is " + footstepIndex);

	if (currentAnim == walkFront || currentAnim == walkBack) {
			if (curFrame == 2) {
//				Debug.Log(footstepIndex);
				if(leavingfootsteps)
					Instantiate(rightfootstepobject,rightfootstepposition.position,rightfootstepposition.rotation);				
				audio.clip = footstepSFX[footstepIndex];
				audio.Play();	
			}
			if( curFrame == 6) {
				if(leavingfootsteps)
					Instantiate(leftfootstepobject,leftfootstepposition.position,leftfootstepposition.rotation);
				audio.clip = footstepSFX[footstepIndex];
				audio.Play();

			}

		}
		else if (currentAnim == walkLeft || currentAnim == walkRight) {
			if (curFrame == 4){ 
				if(leavingfootsteps)
					Instantiate(rightfootstepobject,rightfootstepposition.position,rightfootstepposition.rotation);				
				audio.clip = footstepSFX[footstepIndex];
				audio.Play();
			}
			if(curFrame == 10) {
//				Debug.Log(footstepIndex);
				if(leavingfootsteps)
					Instantiate(leftfootstepobject,leftfootstepposition.position,leftfootstepposition.rotation);
				audio.clip = footstepSFX[footstepIndex];
				audio.Play();
			}
		}
}