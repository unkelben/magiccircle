﻿#pragma strict

public var firework:Transform;
public var movingscript:MoveAround;
public var spritingscript:ChangeSpriteOrientation;

private var triggered = false;
private var fireworkinstance:Transform;

function Start () {

}

function OnTriggerEnter(other:Collider){

	if((other.gameObject.tag=="guy")&&(triggered==false)){
		Debug.Log("entering");
		triggered = true;
		positionguy();
		fireworkinstance = Instantiate(firework, transform.position, transform.rotation);

	}

}



function OnTriggerExit(other:Collider){
	
	if((other.gameObject.tag=="guy")&&(triggered==true)){
		Debug.Log("exiting");
		triggered = false;
		Destroy(fireworkinstance.gameObject);

	}

}

function positionguy(){
	Debug.Log("go to position");
	movingscript.stopMoveTo();
	//movingscript.interrupt = true;
	//yield WaitForSeconds(0.01);
	movingscript.startMoveTo(transform.position);
	movingscript.autopilot=true;
	//movingscript.faceCamera();

}

function Update () {

}