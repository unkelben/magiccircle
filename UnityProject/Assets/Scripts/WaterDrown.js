﻿#pragma strict
public var drownspeed:float;
public var movescript:MoveAround;
public var rollcredits:RollCredits;
public var rollcredits2:RollCredits;
public var splashSFX:AudioClip;
public var creditsDelay:float = 5.0f;

function Start () {

}

function OnTriggerEnter(other:Collider) {
	
	//Debug.Log("tag: "+collisionInfo.gameObject.tag);
	if(other.gameObject.tag=="water"){
		GetComponent.<Rigidbody>().velocity = Vector3.zero;
		GetComponent.<MoveAround>().stopMoveTo();
		GetComponent.<MoveAround>().moving = false;
		Debug.Log("drowing");
		Physics.gravity = new Vector3(0,-1.0*drownspeed,0);
		movescript.enabled = false;
		yield WaitForSeconds(0.2);
		GetComponent.<AudioSource>().clip = splashSFX;
		GetComponent.<AudioSource>().Play();
		yield WaitForSeconds(creditsDelay);
		rollcredits.StartCredits();
		rollcredits2.StartCredits();
	}
}

function Update () {

}