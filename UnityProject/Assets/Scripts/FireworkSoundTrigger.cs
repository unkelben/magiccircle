﻿using UnityEngine;
using System.Collections;

class FireworkSoundTrigger : MonoBehaviour
{
	private AudioSource pop_sfx; 

    ParticleSystem m_particleSystem;
    ParticleSystem.Particle[] m_particles;
    float[] m_times;

    void Awake()
    {
		pop_sfx = GetComponent<AudioSource>();
        m_particleSystem = GetComponent<ParticleSystem>();
        m_times = new float[m_particleSystem.maxParticles];
        m_particles = new ParticleSystem.Particle[m_particleSystem.maxParticles];
    }

    void LateUpdate()
    {
        m_particleSystem.GetParticles(m_particles);
        for (int i = 0; i < m_particles.Length; ++i)
        {
            if (m_times [i] < m_particles [i].remainingLifetime && m_particles [i].remainingLifetime > 0)
            {
                // Birth
                StartCoroutine(Play(m_particles [i].remainingLifetime));
            }
 
            m_times [i] = m_particles [i].remainingLifetime;
 
        }
    }

    IEnumerator Play(float lifetime)
    {
        yield return new WaitForSeconds(lifetime);
        // Death (If spawning a sub-emitter on death, this is when that is triggered)

        pop_sfx.Play();
    }
}