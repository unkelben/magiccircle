﻿#pragma strict

public var wind:Vector3;
public var windparticles:GameObject;
public var tree:Animator;
public var windsound:AudioSource;
public var softwind:AudioClip;
public var strongwind:AudioClip;

function Start () {
    //startwind();
}

public function WindGust(){
    wind = new Vector3(30,10,-30);
    windsound.clip = strongwind;
    windsound.Play();
}

public function startwind(){
    windparticles.SetActive(true);
    windsound.enabled = true;
    windsound.volume = 1;
    //tree.enabled = true;
    tree.SetBool("wind",true);
}

public function stopwind(time){
    windparticles.SetActive(false);
    fadeOutSound(time);
    tree.SetBool("wind",false);
   // tree.enabled = false;
    enabled = false;
    
}

function fadeOutSound(dieTime:float){
	//yield WaitForSeconds(2);
	while(windsound.volume>0){
		windsound.volume-=0.01f;
		yield WaitForSeconds(dieTime * 0.01f);
	}

}

function Update () {
	if(GetComponent.<MoveAround>().hasFallen)
		transform.Translate(wind*Time.deltaTime	,Space.World);

}