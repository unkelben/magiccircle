﻿#pragma strict
public var minimumDistance:float = 1.25f;
public var frontBackSpeed:float;
public var leftRightSpeed:float;
public var boredomSpeedModifier:float = 0.25f;
private var currentFrontBackSpeed:float;
private var currentLeftRightSpeed:float;

private var speed:float;
public var moving:boolean=false;
public var interrupt:boolean = false;
public var userInterrupt:boolean = false;
private var rb:Rigidbody;
public var orientationScript:ChangeSpriteOrientation;
public var hasFallen = false;
public var thrower:ballthrower;
public var lightning:GameObject;
public var mainlight:GameObject;
public var followspot:GameObject;
private var lightcolor:Color;
public var boredomReverbZone:AudioReverbZone;
public var lightningaudio:AudioClip;
public var lightningforce:float;
public var lightningStrikeDelayMinimum:float = 1f;
public var lightningStrikeDelayRange:float = 2f;
public var initialimpulse:float;
public var ReadyToChangeScene:boolean = false;
private var ChangingScene:boolean = false;
public var FadingOutScene:boolean = false;
public var NextScene:String;
public var autopilot:boolean = false;
public var freeze:boolean = false;
public var screenfadeout:UnityEngine.UI.Graphic;
public var preFadeOutDelay:float = 2;
public var fadeOutTime:float = 2;
private var lightningflag:boolean = false;
private var boredomflag:boolean = false;
public var grounded:boolean = false;
public var groundcheck:Transform;
private var hitbyball:boolean = false;


function Start () {
	currentFrontBackSpeed = frontBackSpeed;
	currentLeftRightSpeed = leftRightSpeed;
	rb = GetComponent.<Rigidbody>();
    //rb.isKinematic = true;
    //if in immersion, push him a little for kinetic consistency with wind gust in last scene
	if(SceneManager.GetActiveScene().name=="Immersion"){
	    rb.AddForce(Vector3(5,0,0)*100,ForceMode.Impulse);
	}
	
	if (boredomReverbZone) boredomReverbZone.enabled = false;
}

function OnCollisionEnter(other:Collision){
	//Debug.Log("touched ground");
    if(!hasFallen){
        GetComponent.<AudioSource>().Play();
        hasFallen = true;
    }
	if((other.gameObject.tag=="marbre")&&(!hitbyball)){
		hitbyball = true;
		freeze = true;
		StopCoroutine("moveTo");
		autopilot = true;
		StartCoroutine("moveTo",Vector3(other.transform.position.x,36.9,other.transform.position.z));
		Debug.Log("guy is on base");
		thrower.throwball();
	}
}

function ChangeLightingTo(destcolor:Color){
	var lightcolor:Color =  mainlight.GetComponent.<Light>().color ;
	var colstep:float = 0;
	while(lightcolor!=destcolor){
		mainlight.GetComponent.<Light>().color = Color.Lerp(lightcolor,destcolor,colstep);
		colstep+=0.1;
		yield WaitForSeconds(0.01);
	}

}

function OnTriggerEnter(other:Collider){
    if(other.gameObject.tag=="boredom"){
    	if(!boredomflag){
	        Debug.Log("guy is bored");
	        followspot.active = true;
	        //mainlight.active = false;
	        StopCoroutine("ChangeLightingTo");
	        StartCoroutine("ChangeLightingTo",new Color(0,0,0));
	        //frontBackSpeed = 1;
	        //leftRightSpeed = 1.5;
	        currentFrontBackSpeed = frontBackSpeed * boredomSpeedModifier;
	        currentLeftRightSpeed = leftRightSpeed * boredomSpeedModifier;
	        orientationScript.animationSpeedModifier = boredomSpeedModifier;
	        orientationScript.hardStep = true;
	        boredomReverbZone.enabled = true;
	        boredomflag = true;
	      }
    }
    if(other.gameObject.tag=="anxiety"){
        Debug.Log("guy is anxious");
        // lightcolor = mainlight.GetComponent.<Light>().color;
        if(!lightningflag){
            lightningflag = true;
            StopCoroutine("ChangeLightingTo");
            StartCoroutine("ChangeLightingTo",new Color(.6,.22,.22));
            //mainlight.GetComponent.<Light>().color = new Color(.6,.22,.22);
            mainlight.GetComponent.<Light>().intensity = 0.6;
        
            StopCoroutine("StruckByLightning");
            StartCoroutine("StruckByLightning");
        }

       // frontBackSpeed = 1;
       // leftRightSpeed = 1.5;
    }
	if(other.gameObject.tag=="flow"){
		Debug.Log("guy is in flow");
		followspot.active = false;
        StopCoroutine("ChangeLightingTo");
        StartCoroutine("ChangeLightingTo",new Color(.75,.66,.58));
        currentFrontBackSpeed = frontBackSpeed;
        currentLeftRightSpeed = leftRightSpeed;
        orientationScript.animationSpeedModifier = 1f;
        boredomReverbZone.enabled = false;
        lightningflag = false;
        boredomflag = false;
        lightning.active = false;
        mainlight.GetComponent.<Light>().intensity = 2.0;
        StopCoroutine("StruckByLightning");
        orientationScript.electrocuted = false;

       // autopilot = false;
        orientationScript.hardStep = false;

	}
    if((other.gameObject.tag=="catchdude") && ReadyToChangeScene && !ChangingScene){

    	Debug.Log("Starting change scene process.");

    	ChangingScene = true;

    	//if in cybertext, have wind die out
        if(GetComponent.<wind>()!=null)
        {
        	Debug.Log("Stopping wind...");
            GetComponent.<wind>().stopwind(1);
            yield WaitForSeconds(1);
            Debug.Log("... done stopping wind.");
        }

        Debug.Log("Prefadeout delay...");
        yield WaitForSeconds(preFadeOutDelay);
        Debug.Log("... done with prefadeout delay");

        Debug.Log("Fading out...");
    	screenfadeout.CrossFadeAlpha(1, fadeOutTime, false);
    	FadingOutScene = true;
    	yield WaitForSeconds(fadeOutTime);
    	Debug.Log("... done fading out.");

    	SceneManager.LoadScene (NextScene);
    }
}


function StruckByLightning(){
	while(true==true){
		
	    lightning.active = true;
		GetComponent.<AudioSource>().clip = lightningaudio;
		GetComponent.<AudioSource>().Play();
	    rb.AddForce(Vector3(0,0,-1)*lightningforce,ForceMode.Impulse);
		moving = false;
		StopCoroutine("moveTo");
		moving = false;
		//autopilot = true;

	    //flicker
	    orientationScript.electrocuted = true;

		yield WaitForSeconds(0.3);

		orientationScript.electrocuted = false;

		//autopilot = false;

		
		lightning.active = false;
		var delay:float = lightningStrikeDelayMinimum + Random.value*lightningStrikeDelayRange;

		yield WaitForSeconds(delay);



	}
}

function Update () {

    grounded = Physics.Linecast(transform.position, groundcheck.position, 1 << LayerMask.NameToLayer("ground"));
    if(!grounded){
    	//Debug.Log("not grounded");
        moving = false;
        if(!autopilot && !freeze)
            StopCoroutine("moveTo");
    }
		
	if (Input.GetMouseButtonDown(0)&&moving&&!autopilot&&!freeze){
	    moving = false;
	    StopCoroutine("moveTo");
		//interruptWalking();
	}
    if (Input.GetMouseButtonDown(0)&&hasFallen&&!autopilot&&!freeze)
    {
	
        var hit:RaycastHit;
        var ray:Ray= Camera.main.ScreenPointToRay(Input.mousePosition);
        var layermask = 1 << 8;
        if (Physics.Raycast(ray,  hit, Mathf.Infinity, layermask))
        {
            
//            Debug.Log(hit.point);
           // transform.LookAt(Vector3(hit.point.x,transform.position.y,hit.point.z));

           var feet = new Vector3(transform.position.x,transform.position.y - GetComponent.<MeshFilter>().mesh.bounds.extents.y,transform.position.z);
           if (Vector3.Distance(feet,hit.point) > minimumDistance) {
            	StartCoroutine("moveTo",hit.point);
           }
        }
        
        //If in cybertext, trigger wind on first move
        if((GetComponent.<wind>()!=null)&&(GetComponent.<wind>().enabled == false))
        {
            GetComponent.<wind>().enabled = true;
            GetComponent.<wind>().startwind();
            
        }

  
    }

}

function interruptWalking(){
	interrupt = true;
	yield WaitForSeconds(0.01);
}

function windgust(){
    moving = false;
    autopilot = true;
    GetComponent.<wind>().WindGust();
    StopCoroutine("moveTo");
    moving = false;
    
}

function faceCamera(){

    transform.LookAt(Camera.main.transform);

}
public function stopMoveTo(){
	StopCoroutine("moveTo");
}
function startMoveTo(destination:Vector3){
	StartCoroutine("moveTo",destination);
}

function moveTo(destination:Vector3){
  //  Debug.Log("moving");
  	
    moving = true;



	//Initial surge of push to move against wind in cybertext scene
        transform.LookAt(Vector3(destination.x,transform.position.y,destination.z));
        rb.AddRelativeForce(Vector3.forward*initialimpulse,ForceMode.Impulse);
        //transform.Translate(Vector3.forward * Time.deltaTime * speed*4);

    while((Vector3.Distance(transform.position,Vector3(destination.x,transform.position.y,destination.z))>0.1)&&!interrupt){
        //Debug.Log("distance:"+Vector3.Distance(transform.position,destination));
        transform.LookAt(Vector3(destination.x,transform.position.y,destination.z));
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        //rb.AddForce(Vector3.forward*speed*Time.deltaTime,ForceMode.VelocityChange);
        		

        if (orientationScript.currentAnim == orientationScript.walkRight || orientationScript.currentAnim == orientationScript.walkLeft) 
        {
    		speed = currentLeftRightSpeed;
	    }
	    else {
	    	speed = currentFrontBackSpeed;
	    }


        yield (WaitForSeconds(0.01));
    }

    if (!autopilot && !freeze && !ReadyToChangeScene) 
    {
    	rb.velocity=Vector3.zero;
   	}
    interrupt = false;
    moving = false;
    autopilot = false;

}