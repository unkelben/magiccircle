﻿#pragma strict

public var balle:Transform;
public var force:float;
public var target:Transform;
public var repeatable = true;
private var thrownOne = false;

function Start () {

	//ballthrower();

}

function throwball(){

	if (repeatable || !thrownOne) {
		var tempballe:Transform = Instantiate(balle,transform.position,transform.rotation);
		tempballe.GetComponent.<Rigidbody>().AddRelativeForce(new Vector3(0,0,1)*force,ForceMode.Impulse);
		thrownOne = true;
	}
}

function Update () {

	transform.LookAt(target);

}