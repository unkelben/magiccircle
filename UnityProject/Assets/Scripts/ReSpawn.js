﻿#pragma strict

public var respawnpoint:Transform;
public var windscript:wind;
public var maxRespawns:int = 3;

private var respawns:int = 0;

function Start () {

}

function Update () {

}

function OnTriggerEnter(col:Collider){

    if(col.gameObject.tag=="guy"){
        if(windscript!=null){
            windscript.stopwind(1);
        }

        col.GetComponent.<MoveAround>().moving = false;
        col.GetComponent.<MoveAround>().stopMoveTo();
        col.GetComponent.<MoveAround>().hasFallen = false;
        col.GetComponent.<AudioSource>().clip = GameObject.Find("Guy").GetComponent.<ChangeSpriteOrientation>().footstepSFX[0];

        if (respawns >= maxRespawns-1 && SceneManager.GetActiveScene().name == "Cybertext") {
        	col.GetComponent.<MoveAround>().ReadyToChangeScene = true;
        }

        if (!col.GetComponent.<MoveAround>().ReadyToChangeScene) {
			col.transform.position = respawnpoint.position;
			respawns++;
		}
	}
	
}