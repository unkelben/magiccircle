﻿#pragma strict

import UnityEngine.SceneManagement;

public var delay:float = 2.0f;

function Start () {
    delayedStart(delay);
}

function Update () {
    
}

function delayedStart(delay:float){
    yield WaitForSeconds(delay);
    SceneManager.LoadScene ("MagicCircle");
}