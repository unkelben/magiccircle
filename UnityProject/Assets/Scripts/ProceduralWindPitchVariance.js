﻿#pragma strict

public var xScale:float = 5f; // The amount of jump in pitch change
public var gustiness:float = 0.01; // How often the wind changes pitch

function Start () {

}

function Update () {
	if (Random.Range(0f,1f) < gustiness)
	{
		GetComponent.<AudioSource>().pitch = 0.75 + Mathf.PerlinNoise(Time.time/xScale, 0.0f);
	}
}