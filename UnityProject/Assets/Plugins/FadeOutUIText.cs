﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeOutUIText : MonoBehaviour
{

    public float delay = 2.0f;
    public float fadeTime = 2.0f;
    public float targetAlpha = 0.0f;

    // Use this for initialization
    public void Start()
    {

        StartCoroutine(StartFade());
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }

   public IEnumerator StartFade()
    {
		if (targetAlpha == 1.0f)
			GetComponent<Graphic>().canvasRenderer.SetAlpha(0.0f);        
		yield return new WaitForSeconds(delay);

        GetComponent<Graphic>().CrossFadeAlpha(targetAlpha, fadeTime, false);
    }
}
