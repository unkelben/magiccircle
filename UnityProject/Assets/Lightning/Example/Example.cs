﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum SelectColor{
	Red = 1,
	Green,
	Blue,
	White,
	Black
}

public class Example : MonoBehaviour {
	public GameObject PrefabLightning;
	public Transform ParentTransform;
	public Transform TrStart;
	public Transform TrEnd;



	GameObject newObjLightning;
	Lightning newLightning = null;

	void Start () {


		CreateLightning();
	}

	void FixedUpdate () {
			newLightning.SetStartPos(TrStart.localPosition);
			newLightning.SetEndPos(TrEnd.localPosition);

	
	}


		
	

	void CreateLightning() {
		if(!newObjLightning) {
			newObjLightning = Instantiate(PrefabLightning);
			newObjLightning.transform.SetParent(ParentTransform);
			newObjLightning.transform.localPosition = Vector3.zero;
			newLightning = newObjLightning.GetComponent<Lightning>();

			
			newLightning.Create(TrStart.localPosition, TrEnd.localPosition);
			
			
		}
	}

	public void SetColor(int c) {
		Color _c = Color.red;

		switch((SelectColor)c){
		case SelectColor.Red:
			_c = Color.red;
			break;
		case SelectColor.Green:
			_c = Color.green;
			break;
		case SelectColor.Blue:
			_c = Color.blue;
			break;
		case SelectColor.White:
			_c = Color.white;
			break;
		case SelectColor.Black:
			_c = Color.black;
			break;
		}

		if(newObjLightning)
			newLightning.ColorLightning = _c;
	}
}
